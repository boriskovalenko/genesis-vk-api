<?php

use Src\RequestManager;

spl_autoload_register(function ($class) {
    $class = str_replace("\\", '/', $class);
    include $class . '.php';
});

require_once __DIR__ . '/vendor/autoload.php';

echo 'Welcome!' . PHP_EOL;
echo 'Please enter the user id (should contains only numbers), or path to .csv file with ids, or type "show" to show all information about imported users:' . PHP_EOL;

$request = fgets(STDIN);
$request = preg_replace("/\r|\n/", '', $request);

//pass user request to Request Manager for processing
$manager = new RequestManager();
$manager->manage($request);
