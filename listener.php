<?php

use Src\SocialQueuesImportReceiver;
use Src\Social\VK\VKSocialProvider;
use VK\Client\VKApiClient;

spl_autoload_register(function ($class) {
    $class = str_replace("\\", '/', $class);
    include $class . '.php';
});

require_once __DIR__ . '/vendor/autoload.php';

$client = new VKApiClient();
$serviceProvider = new VKSocialProvider($client);
$receiver = new SocialQueuesImportReceiver($serviceProvider);
$receiver->listen();
