<?php

declare(strict_types = 1);

namespace Src\Social\VK;

use Src\Social\SocialProviderInterface;
use Src\Social\Model\Album;
use Src\Social\Model\Photo;
use Src\Social\Model\User;
use VK\Client\VKApiClient;

class VKSocialProvider implements SocialProviderInterface
{
    private $config;
    private $accessToken;
    private $vk;

    public function __construct(VKApiClient $client)
    {
        $this->config = parse_ini_file('config.ini');
        $this->accessToken = $this->config['vk_access_token'];
        $this->vk = $client;
    }

    public function findUser(string $userId): User
    {
        $apiResponse = $this->vk->users()->get($this->accessToken, array(
            'user_ids' => $userId
        ));

        $user = new User($apiResponse[0]['id'], $apiResponse[0]['first_name'], $apiResponse[0]['last_name']);

        return $user;
    }

    public function findAlbums(string $userId): array
    {
        $apiResponse = $this->vk->photos()->getAlbums($this->accessToken, array(
            'owner_id' => $userId
        ));

        $albums = [];

        foreach ($apiResponse['items'] as $item) {
            $album = new Album($item['id'], $item['owner_id'], $item['title']);
            $albums[] = $album;
        }

        return $albums;
    }

    public function findPhotos(string $userId, int $albumId): array
    {
        $apiResponse = $this->vk->photos()->get($this->accessToken, array(
            'owner_id' => $userId,
            'album_id' => $albumId,
        ));

        $photos = [];

        foreach ($apiResponse['items'] as $item) {
            foreach ($item['sizes'] as $size) {
                if (isset($size['url'])) {
                    $item['url'] = $size['url'];
                }
            }
            $photo = new Photo($item['id'], $item['owner_id'], $item['album_id'], $item['url']);
            $photos[] = $photo;
        }
        return $photos;
    }

}