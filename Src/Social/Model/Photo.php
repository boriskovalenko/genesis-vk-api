<?php

declare(strict_types = 1);

namespace Src\Social\Model;

class Photo
{
    private $id;
    private $ownerId;
    private $albumId;
    private $url;

    public function __construct(int $id, int $ownerId, int $albumId, string $url)
    {
        $this->id = $id;
        $this->ownerId = $ownerId;
        $this->albumId = $albumId;
        $this->url = $url;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function getAlbumId(): int
    {
        return $this->albumId;
    }

    public function getURL(): string
    {
        return $this->url;
    }
}