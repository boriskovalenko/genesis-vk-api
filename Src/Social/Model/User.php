<?php

declare(strict_types = 1);

namespace Src\Social\Model;

class User
{
    private $id;
    private $firstName;
    private $lastName;

    public function __construct(int $id, string $firstName, string $lastName)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getFName(): string
    {
        return $this->firstName;
    }

    public function getLName(): string
    {
        return $this->lastName;
    }
}