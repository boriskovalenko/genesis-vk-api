<?php

declare(strict_types = 1);

namespace Src\Social\Model;

class Album
{
    private $id;
    private $ownerId;
    private $title;

    public function __construct(int $id, int $ownerId, string $title)
    {
        $this->id = $id;
        $this->ownerId = $ownerId;
        $this->title = $title;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}