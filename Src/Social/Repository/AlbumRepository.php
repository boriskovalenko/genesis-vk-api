<?php

declare(strict_types = 1);

namespace Src\Social\Repository;

use Src\Social\Model\Album;

class AlbumRepository extends AbstractRepository
{
    public function saveObject(Album $album)
    {
        $query = "INSERT IGNORE INTO `albums`(`external_id`, `owner_id`, `title`) VALUES (:external_id, :owner_id, :title)";

        $sth = $this->dbh->prepare($query);

        $sth->execute(array(':external_id' => $album->getId(), ':owner_id' => $album->getOwnerId(), ':title' => $album->getTitle()));
    }

    public function getAlbums(int $ownerId): array
    {
        $query = "SELECT `external_id`, `owner_id`, `title` FROM `albums` WHERE `owner_id` = :owner_id";
        $sth = $this->dbh->prepare($query);
        $sth->execute(array(':owner_id' => $ownerId));
        $result =$sth->fetchAll();

        $albums = [];

        if (!empty($result)) {
            foreach ($result as $item) {
                $albums[] = new Album(intval($item['external_id']), intval($item['owner_id']), $item['title']);
            }
        }
        return $albums;
    }
}