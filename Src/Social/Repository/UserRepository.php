<?php

declare(strict_types = 1);

namespace Src\Social\Repository;

use Src\Social\Model\User;

class UserRepository extends AbstractRepository
{
    public function saveObject(User $user)
    {
        $query = "INSERT IGNORE INTO `users`(`external_id`, `first_name`, `last_name`) VALUES (:external_id, :first_name, :last_name)";

        $sth = $this->dbh->prepare($query);

        $sth->execute(array(':external_id' => $user->getId(), ':first_name' => $user->getFName(), ':last_name' => $user->getLName()));

    }

    public function getUser(string $userId)
    {
        $query = "SELECT `external_id`, `first_name`, `last_name` FROM `users` WHERE `external_id` = :external_id";
        $sth = $this->dbh->prepare($query);
        $sth->execute(array(':external_id' => $userId));
        $result = $sth->fetchAll();
        if (!empty($result)) {
            $user = new User(intval($result[0]['external_id']), $result[0]['first_name'], $result[0]['last_name']);
            return $user;
        } else {
            return null;
        }
    }
}