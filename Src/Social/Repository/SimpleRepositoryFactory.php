<?php

namespace Src\Social\Repository;

use Src\Social\Repository\AlbumRepository;
use Src\Social\Repository\PhotoRepository;
use Src\Social\Repository\UserRepository;

class SimpleRepositoryFactory
{
    private $dbName;
    private $host;
    private $user;
    private $pass;

    public function __construct(string $dbName, string $host, string $user, string $pass)
    {
        $this->dbName = $dbName;
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
    }

    public function createUserRepository()
    {
        return new UserRepository($this->dbName, $this->host, $this->user, $this->pass);
    }

    public function createAlbumRepository()
    {
        return new AlbumRepository($this->dbName, $this->host, $this->user, $this->pass);
    }

    public function createPhotoRepository()
    {
        return new PhotoRepository($this->dbName, $this->host, $this->user, $this->pass);
    }

}