<?php

declare(strict_types = 1);

namespace Src\Social\Repository;

use Src\Social\Model\Photo;

class PhotoRepository extends AbstractRepository
{
    public function saveObject(Photo $photo)
    {
        $query = "INSERT IGNORE INTO `photos`(`external_id`, `owner_id`, `album_id`, `url`) VALUES (:external_id, :owner_id, :album_id, :url)";

        $sth = $this->dbh->prepare($query);

        $sth->execute(array(':external_id' => $photo->getId(), ':owner_id' => $photo->getOwnerId(), ':album_id' => $photo->getAlbumId(), ':url' => $photo->getURL()));
    }

    public function getPhotos(int $albumId): array
    {
        $query = "SELECT `external_id`, `owner_id`, `album_id`, `url` FROM `photos` WHERE `album_id` = :album_id";
        $sth = $this->dbh->prepare($query);
        $sth->execute(array(':album_id' => $albumId));
        $result =$sth->fetchAll();

        $photos = [];

        if (!empty($result)) {
            foreach ($result as $item) {
                $photos[] = new Photo(intval($item['external_id']), intval($item['owner_id']), intval($item['album_id']), $item['url']);
            }
        }
        return $photos;
    }
}