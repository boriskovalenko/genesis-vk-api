<?php

declare(strict_types = 1);

namespace Src\Social\Repository;

abstract class AbstractRepository
{
    protected $dbh;

    public function __construct(string $dbName, string $host, string $user, string $pass)
    {
        $dsn = 'mysql:dbname=' . $dbName . ';host=' . $host;

        try {
            $dbh = new \PDO($dsn, $user, $pass);
            $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->dbh = $dbh;
        } catch (\PDOException $e) {
            throw $e;
        }
    }
}