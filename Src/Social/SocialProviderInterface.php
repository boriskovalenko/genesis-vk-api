<?php

namespace Src\Social;

use Src\Social\Model\User;

interface SocialProviderInterface
{
    public function findUser(string $userId): User;

    public function findAlbums(string $userId): array;

    public function findPhotos(string $userId, int $albumId): array;

}