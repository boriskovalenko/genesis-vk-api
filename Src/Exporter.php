<?php

declare(strict_types = 1);

namespace Src;

use Src\Social\Repository\AlbumRepository;
use Src\Social\Repository\PhotoRepository;
use Src\Social\Repository\UserRepository;

class Exporter
{
    private $userRepository;
    private $albumRepository;
    private $photoRepository;

    public function __construct(UserRepository $userRepository, AlbumRepository $albumRepository, PhotoRepository $photoRepository)
    {
        $this->userRepository = $userRepository;
        $this->albumRepository = $albumRepository;
        $this->photoRepository = $photoRepository;
    }

    public function getImportedData(string $userId)
    {
        $user = $this->userRepository->getUser($userId);
        if ($user) {
            echo 'User: ' . $user->getFName() . ' ' . $user->getLName() . PHP_EOL;
            $albums = $this->albumRepository->getAlbums($user->getId());
            foreach ($albums as $album) {
                echo 'Album: ' . $album->getTitle() . PHP_EOL;
                $photos = $this->photoRepository->getPhotos($album->getOwnerId());
                foreach ($photos as $photo) {
                    echo 'Photo: ' . $photo->getUrl() . PHP_EOL;
                }
            }
        } else {
            echo "No such user in Database";
        }

    }
}