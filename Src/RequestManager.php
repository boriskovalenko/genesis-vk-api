<?php

declare(strict_types = 1);

namespace Src;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Src\Social\Repository\SimpleRepositoryFactory;

class RequestManager
{
    private $config;

    public function __construct()
    {
        $this->config = parse_ini_file('config.ini');
    }

    public function manage(string $request)
    {
        if (empty($request)) {
            echo 'Empty request';
        } elseif ($request == 'show') {
//          if user want to see imported data for some user
            echo 'Please enter the user id, to show imported information about him:'.PHP_EOL;

            $userId = fgets(STDIN);
            $userId = preg_replace("/\r|\n/", '', $userId);

//          display imported data
            $repoFactory = new SimpleRepositoryFactory(
                $this->config['db_name'],
                $this->config['db_host'],
                $this->config['db_user'],
                $this->config['db_password']
            );
            $exporter = new Exporter(
                $repoFactory->createUserRepository(),
                $repoFactory->createAlbumRepository(),
                $repoFactory->createPhotoRepository()
            );
            $exporter->getImportedData($userId);
        } else {
            $ids = [];
            if (file_exists($request)) {
//              parse csv file to array
                $file = fopen($request, "r");
                while (($line = fgetcsv($file, 30)) !== false)
                {
                    $ids[] = $line[0];
                }
                $this->addQueue($ids);
            } elseif (is_numeric($request)) {
                $ids[] = $request;
                $this->addQueue($ids);
            } else {
                echo 'Sorry. Wrong type of user_id or not existing file path'.PHP_EOL;
            }
        }
    }

    private function addQueue(array $ids)
    {
        $idString = json_encode($ids);

//      RabbitMQ connection
        $connection = new AMQPStreamConnection(
            $this->config['amqp_host'],
            $this->config['amqp_port'],
            $this->config['amqp_user'],
            $this->config['amqp_password']
        );
        $channel = $connection->channel();

        $channel->queue_declare('import', false, false, false, false);

        $msg = new AMQPMessage($idString);
        $channel->basic_publish($msg, '', 'import');

        $channel->close();
        $connection->close();
    }
}