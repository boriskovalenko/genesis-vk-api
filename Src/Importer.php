<?php

declare(strict_types = 1);

namespace Src;

use Src\Social\Repository\AlbumRepository;
use Src\Social\Repository\PhotoRepository;
use Src\Social\Repository\UserRepository;
use Src\Social\SocialProviderInterface;

class Importer
{
    private $socialProvider;
    private $userRepository;
    private $albumRepository;
    private $photoRepository;

    public function __construct(SocialProviderInterface $socialProvider, UserRepository $userRepository, AlbumRepository $albumRepository, PhotoRepository $photoRepository)
    {
        $this->socialProvider = $socialProvider;
        $this->userRepository = $userRepository;
        $this->albumRepository = $albumRepository;
        $this->photoRepository = $photoRepository;
    }

    public function import(string $ids)
    {
        $ids = json_decode($ids);
        foreach ($ids as $userId) {
            try {
                $user = $this->socialProvider->findUser($userId);
                $albums = $this->socialProvider->findAlbums($userId);
                if (isset($user) && !empty($albums)) {
                    foreach ($albums as $album) {
                        $photos = $this->socialProvider->findPhotos($userId, $album->getId());
                        if (!empty($photos)) {
                            $this->userRepository->saveObject($user);
                            $this->albumRepository->saveObject($album);
                            foreach ($photos as $photo) {
                                $this->photoRepository->saveObject($photo);
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
    }
}