<?php

namespace Src;

use PhpAmqpLib\Connection\AMQPStreamConnection;

use Src\Social\Repository\SimpleRepositoryFactory;
use Src\Social\SocialProviderInterface;

class SocialQueuesImportReceiver
{
    private $dataProvider;
    private $config;

    public function __construct(SocialProviderInterface $dataProvider)
    {
        $this->config = parse_ini_file('config.ini');
        $this->dataProvider = $dataProvider;
    }

    public function listen()
    {
        $connection = new AMQPStreamConnection(
            $this->config['amqp_host'],
            $this->config['amqp_port'],
            $this->config['amqp_user'],
            $this->config['amqp_password']
        );
        $channel = $connection->channel();
        $channel->queue_declare('import', false, false, false, false);
        $channel->basic_consume('import', '', false, true, false, false, array($this, 'processQueue'));

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    public function processQueue($msg)
    {
        $repoFactory = new SimpleRepositoryFactory(
            $this->config['db_name'],
            $this->config['db_host'],
            $this->config['db_user'],
            $this->config['db_password']
        );
        $importer = new Importer(
            $this->dataProvider,
            $repoFactory->createUserRepository(),
            $repoFactory->createAlbumRepository(),
            $repoFactory->createPhotoRepository()
        );
        $importer->import($msg->body);
    }
}